﻿using UnityEngine;

public enum PlayerKind
{
    Knight,
    Assesin,
    Bard,
    Barbarian,
    Smeaggle
}
public class DataPlayer : MonoBehaviour
{
    // Properties
    [SerializeField]
    public new string name;
    public string surname;
    public float height;
    public float width;
    public float weight;
    public int speed;
    public int distanceToRun;

    int counterFrame;    
    float ananimationFrame;
    float frame2Change;
    float currentFrameRate;
    private SpriteAnimation SpAnimation;
    private SpriteHability SpHability;
    public int lifes;
    public PlayerKind Kind;    
    public Sprite[] sprite = new Sprite[3];

    // Start is called before the first frame update
    void Start()
    {
        //Application.targetFrameRate = 12;
        SpHability = GetComponent<SpriteHability>();
        SpAnimation = GetComponent<SpriteAnimation>();
    }

    // Update is called once per frame
    void Update()
    {
        setFrame();
        SpAnimation.MoveSprite();
        if (Input.GetKey(KeyCode.Space)) {
            SpHability.ChangeSize();
        }
    }
   
    void setFrame() {
        currentFrameRate = Time.deltaTime;
        ananimationFrame = currentFrameRate / 12;
        counterFrame++;
        frame2Change = currentFrameRate / ananimationFrame;
        if (counterFrame%frame2Change == 0) {
            Application.targetFrameRate = (int) frame2Change;
            counterFrame = 0;
        }
    }
}
