using UnityEngine;

public class EnemyCollitionControles : MonoBehaviour
{
    [SerializeField]
    private RaycastHit2D hit;
    private int scorePoints = 5;
    private Vector3 mousePos;
    private Vector2 mousePos2D;


    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePos2D = new Vector2(mousePos.x, mousePos.y);
            hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider == null)
            {
                Debug.Log("Bad hit");
            }
            else if (hit.collider.CompareTag("Enemy"))
            {
                GameManager.Instance.InCreaseScore(scorePoints);
                Destroy(gameObject);
                GameManager.Instance.noEnemies--;
            }

        }
    }
    // Collition Controler with enemies and player
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            // Aplicar da�o
            GameObject plLifes = collision.gameObject;
            plLifes.GetComponent<DataPlayer>().lifes--;
            if (plLifes.GetComponent<DataPlayer>().lifes < 0)
            {
                Destroy(plLifes.gameObject);
                GameManager.Instance.EndGame();
            }

        }
        Destroy(gameObject);
        GameManager.Instance.noEnemies--;
    }
}
