using UnityEngine;

public class EnemyMoveController : MonoBehaviour
{
    // Properties
    public int speedEnemy;
    private Transform player;
    private Rigidbody2D rb;
    private Vector2 movement;

    void Start()
    {
        rb = this.GetComponent<Rigidbody2D>();
        player = GameObject.Find("Player").GetComponent<Transform>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player) {
            Vector3 direction = player.position - transform.position;
            direction.Normalize();
            movement = direction;
        }
    }
    private void FixedUpdate()
    {
        followPlayer(movement);
    }

    void followPlayer(Vector2 direction) {
        rb.MovePosition((Vector2)transform.position + (direction * speedEnemy * Time.deltaTime));
    }
}
