using UnityEngine.SceneManagement;
using UnityEngine;


public class GameManager : MonoBehaviour
{
    public GameObject player;
    private static GameManager instance;
    public static UIController UIcontroller;
    public int noEnemies;
    private int _score;
    public int score
    {
        get { return _score; }
    } 

    public static GameManager Instance {
        get {            
            return instance;
        }
    }

    private void Awake()
    {
        if (instance != null && instance != this) {
            Destroy(this.gameObject);
        }

        instance = this;
    }

    void Start()
    {
        _score = 0;
        noEnemies = 0;

        player = GameObject.Find("Player");
        UIcontroller = GameObject.Find("Canvas").GetComponent<UIController>();
    }

    bool gameHasEnded = false;
    public void EndGame() {
        if (!gameHasEnded) {
            Debug.Log("GAME OVER");
            Invoke(nameof(Restart), 1f);
        }
    }

    void Restart() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name); 
    }

    public void InCreaseScore(int increment) {
        _score += increment;
    }
}

