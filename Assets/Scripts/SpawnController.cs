using UnityEngine;

public class SpawnController : MonoBehaviour
{
    // Properties
    [SerializeField]
    private GameObject enemy;
    public Vector2 randomX;
    public Vector2 randomY;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating(("CreateEnemy"),1,2);
    }

    // Update is called once per frame
    void Update()
    {
    }

    void CreateEnemy()
    {
        Instantiate(enemy, new Vector3(Random.RandomRange(randomX.x, randomX.y), Random.RandomRange(randomY.x, randomY.y), 0), Quaternion.identity);
        GameManager.Instance.noEnemies += 1;
    }
}
