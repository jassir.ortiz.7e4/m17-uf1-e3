﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteAnimation : MonoBehaviour
{
    // Properties 
    bool collisionLeft;
    bool collisionRight;
    private int currentSprite;
    private SpriteRenderer renderes;
    private Sprite[] sprite = new Sprite[3];
    private DataPlayer Dp;

    // Start is called before the first frame update
    public void Start()
    {        
        currentSprite = 0;
        collisionLeft = true;
        collisionRight = false;
        renderes = gameObject.GetComponent<SpriteRenderer>();
        Dp = GetComponent<DataPlayer>();
        if (Dp.weight != 0)
        {
            Dp.speed = ((int)((Dp.speed - Dp.weight)) / 2);
        }
    }

    // Methods 
    public void MoveSprite()
    {   
        if (transform.position.x < Dp.distanceToRun * -1 == false)
        {
            AutoMoveSprite();          
        }
        else
        {
            collisionLeft = true;
            collisionRight = false;
        }
        if (transform.position.x > Dp.distanceToRun == false)
        {
            AutoMoveSprite();        
        }
        else
        {
            collisionRight = true;
            collisionLeft = false;
        }
        ChangeSprite();
    }

    void ChangeSprite()
    {
        currentSprite++;
        renderes.sprite = Dp.sprite[currentSprite];
        if (currentSprite == 2) { currentSprite = 0; }
    }

    void AutoMoveSprite()
    {
        switch (collisionLeft)
        {
            case true:
                transform.position += Vector3.right * Dp.speed * Time.deltaTime;
                renderes.flipX = Dp.sprite.IsFixedSize;
                break;
        }
        switch (collisionRight)
        {
            case true:
                transform.position += Vector3.left * Dp.speed * Time.deltaTime;
                renderes.flipX = Dp.sprite.IsFixedSize.Equals(-1);
                break;
        }
    }
}
