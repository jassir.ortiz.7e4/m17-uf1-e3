﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteHability : MonoBehaviour
{
    // Properties
    private DataPlayer Dp;
    private Vector2 scaleSprite;

    // Start is called before the first frame update
    void Start()
    {
        scaleSprite = new Vector2(transform.localScale.x, transform.localScale.y);
        Dp = GetComponent<DataPlayer>();
    }

    public void ChangeSize() {
        if (transform.localScale.x < Dp.width && transform.localScale.y < Dp.height)
        {
            transform.localScale = new Vector2(Dp.width, Dp.height);
        }
        else if (transform.localScale.x == Dp.width && transform.localScale.y == Dp.height)
        {
            transform.localScale = scaleSprite;
        }
    }
}
