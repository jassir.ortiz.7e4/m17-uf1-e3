using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    // Properties 
    public DataPlayer player;
    public GameObject playerGO;
    public Text textFrames;
    public Text textLifes;
    public Text textNoEnemys;
    public Text textScore;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.Find("Player").GetComponent<DataPlayer>();
    }

    // Update is called once per frame
    void Update()
    {
        UIControl();
    }

    void UIControl()
    {
        textLifes.text = "Vidas: " + player.lifes;
        textFrames.text = "FPS: " + Application.targetFrameRate;
        textNoEnemys.text = "Enemigos en escena: " + GameManager.Instance.noEnemies;
        textScore.text = "Score: " + GameManager.Instance.score;
    }
}
